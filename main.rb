# frozen_string_literal: true

class TicTacToe
  MARKS = %w[X O].freeze
  attr_reader :turn, :marks

  def initialize(player1_mark, player2_mark)
    @marks = [player1_mark, player2_mark]
    @turn = rand 2
    @table = Array(1..9)
    @rooms = 9
    @winner = -1
  end

  def over?
    [0, 1, 2].each do |i|
      if @table[i] == @table[i + 3] && @table[i + 3] == @table[i + 6]
        @winner = @marks.index @table[i]
        return true
      end
    end
    
    [0, 3, 6].each do |i|
      if @table[i] == @table[i + 1] && @table[i + 1] == @table[i + 2]
        @winner = @marks.index @table[i]
        return true
      end
    end
    
    [0, 2].each do |i|
      if @table[i] == @table[4] && @table[4] == @table[8 - i]
        @winner = @marks.index @table[i]
        return true
      end
    end
    
    return true if @rooms < 2
    
    false
  end

  def table
    format("   │   │   \n %s │ %s │ %s \n   │   │   \n───┼───┼───\n   │   │   \n %s │ %s │ %s \n   │   │   \n───┼───┼───\n   │   │   \n %s │ %s │ %s \n   │   │   \n", *@table)
  end

  def winner
    @winner == -1 ? "It's a draw" : format("Player %i wins", @winner + 1)
  end

  def play_turn(position)
    raise RangeError, 'Position out of bounds' unless position.between? 1, 9
    raise ArgumentError, 'Position already choosed' if MARKS.include? @table[position - 1]

    @table[position - 1] = @marks[@turn]
    @turn = (@turn + 1) % 2
    @rooms -= 1
  end
end

ACCEPT = %w[Y YES].freeze
PROMPT = '>>> '

answer = ''.dup
puts 'Welcome to the Tic Tac Toe!'
puts 'Player 1, choose your mark: X or O?'
until TicTacToe::MARKS.include? answer.upcase!
  print PROMPT
  answer = gets.chomp
end

puts `clear`
game = TicTacToe.new answer, answer == 'X' ? 'O' : 'X'
loop do
  puts game.table
  puts format('Player %i turn!', game.turn + 1)
  puts 'Select a position between 1 to 9.'
  loop do
    print PROMPT
    answer = gets.chomp
    begin
      game.play_turn answer.to_i
      break
    rescue StandardError => e
      puts e.message
    end
  end

  puts `clear`
  next unless game.over?

  puts game.table
  puts game.winner
  puts 'Wanna play again? [N/y]'
  print PROMPT
  answer = gets.chomp
  break unless ACCEPT.include? answer.upcase!

  puts `clear`
  game = TicTacToe.new(*game.marks)
end
